year = int(input("Enter a year: "))

if(year % 4 == 0 ):
	print("Year is a leap year")
else:
	print("Year is not a leap year")

num1 = int(input("Enter number of rows: "))
num2 = int(input("Enter number of Columns: "))

for rows in range(num1):
	for columns in range(num2):
		print("*", end = '')
	print("")